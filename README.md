# FCOE Drupal 8 Theme
This is the default Drupal 8 theme for FCOE.org

## Local Build Process
Local development requires a vanilla drupal install, followed by a site-install using the fcss-subsite profile, to do so, use the following steps:

### 1. Download Drupal base, either by downloading the latest version, or using the following composer or drush commands
```bash
sudo composer create-project drupal/drupal:8.2.6 d8vapa --stability dev --no-interaction
```
or
```bash
drush dl drupal --select
```
### 2. Install the site using the following command
```bash
drush site-install fcss-subsite --db-url='mysql://user:password@localhost/dbname' --site-name=sitename
```

You must use the fcss-subsite profile in order to incorporate all the necessary themes and modules. If you chose to forego using composer for a local install, you need to move all custom modules (features and site-provided modules) and custom themes manually, before running the site-install.

If you chose to not use the fcss-subsite profile, use the Initial Configuration list below to begin development:

## Initial Configuration

#### 1. Enable all neccessary contrib modules and custom modules (excluding features)  
```bash
drush en ctools twig_tweak CKEditorChanges MenuRenderModule kint entity_reference_revisions crop image_widget_crop plupload plupload_widget title components config_update config_update_ui pathauto token paragraphs
```
Rebuild the cache for the site
```bash
drush cache-rebuild
```
#### 2. Enable and set as default the fcoe_subsite theme
#### 3. Enable all FCOE D8 Features set within the 'FCOE D8' module group
#### 4. Configure blocks
  * Remove all blocks, with the exception of the main navigation, from the block layout
  * Place the hero image block in the homepage primary region
  * Place 1 to 3 custom blocks into the homepage secondary region
  * Place any applicable custom blocsk into the sidebar

## Adding and Updating Features

All new configuration must be stored in features. In order to add features, using the following steps.

1. Package features by type (block,content,etc) and limit to only the required fields and form displays
2. Remove any conflicts
3. Remove any references to fields that are shared with other Features
4. Initialize a repo in the FCOE D8 Features folder on bitbucket
5. Navigate to the custom feature folder in the modules directory, add upstream to the folder, initialize git, and push to the repo
6. Pull the repo down into the custom/features folder within the modules of the necessary platform
7. Activate the plugin within the backend of the sites on the platform

Updating featues requires synchronizing configuration, and migrating content.

1. Follow steps 1 through 3 of the above directions for adding a features
2. Create a new branch for the feature
3. Push those changes to the new branch
4. In a local environment, update the feature, then use the Configuration UI Manager module to synchronize configuration
5. Migrate any content if needed using the migrate module

Content Synchronization and Migration would need to occur on all sites where the new platform exists. Migration may require pulling down a current version of the site, migrating content, then pushing changes through a database import on the remote server. 

## Theme development

### Environment Requirements

This theme is built with Foundation 6. To run it, you'll need the following

  * [Node.js](http://nodejs.org): Use the installer provided on the NodeJS website.
  * [Gulp](http://gulpjs.com/): Run `[sudo] npm install -g gulp`
  * [Bower](http://bower.io): Run `[sudo] npm install -g bower`

### Module Requirements

  * [Twig Tweak](http://www.drupal.org/twig_tweak): A module for custom twig extensions
  * [Component Library](http://www.drupal.org/components): A module for allow custom component libraries
  * [CKEditor Changes](http://www.drupal.org/components): A custom ckeditor config module
  * [MenuRenderModule](http://www.drupal.org/components): A custom menu rendering module

### Installation

```bash
git clone https://USERNAME@bitbucket.org/fcoe-webservices/fcss_dept.git
npm install && bower install
```

## Configuring for Development

In order to test and run browsersync, the twig cache needs to be disabled and asset aggregation needs to be deactivated.
#### Deactivate CSS/JS aggregation
Create a `local.settings.php` file, and uncomment the following lines

```
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;
```

#### Disable the Twig Cache, Set Debug to true
update the following parameters in the `development.services.yml` file

```
parameters:
  twig.config:
    debug : true
    auto_reload: true
    cache: false
```

for more information, [review this documentation](https://www.drupal.org/node/2598914)

## Theme Architecture

The fcss subsite theme uses component library architecture to separate front end markup and styles, from drupal-specific templating logic. Ths allows front end design components to be distributed across multiple content types or configurations, giving developers the ability to change design globally. 

### Drupal Templates
All twig files are organized within subfolders, similar to the classy or seven theme, according to purpose. Use the [D8 Theme structure](https://www.drupal.org/theme-guide/8/folder-structure) as a guideline Those twig files then reference components.

### Component Styles/Markup
Whiplash uses The [Drupal 8 CSS File organization guidelines](https://www.drupal.org/node/1887922) to organize styles within the component folders.

  * `base/`: all global styles for elements. Foundation controls most of the base styles, and are called in through the foundation config file.
  * `layout/`: styles for arrangement.
  * `components/`: discrete UI elements. The majority of styles reside in this folder.
  * `state/` : client-specific styles. Primarily print styles and javascript styles
  * `utils/` : base configuration values not included in foundation. Mixins and helpers not included in foundation
  * `vendor/` : any styles from 3rd party libraries or vendors. Includes foundation's config
  * `app.scss` : pulls in foundation, configuration files, and all partials from the `base`,`layout`,`components`, and `state` folders

### Component Configuration
Each design component contains both a twig and scss file, which is referenced within the twig file. Information is passed into the twig files, with the proper tag names, by templates in the `/templates` folder.

Each new SCSS file referenced in a twig file requires a component library to be added to the `libraries.yml` file. 

### SCSS Builds
Configuration is handled in the following order
1. Glob import base config files from the `utils/` folder
2. Import foundation config through the `vendor/foundation_config` file
3. Import foundation
4. Glob import `base`,`layout`,`components`, and `state` folders
5. Import the shame partial

## Build Process
Whiplash uses gulp as its build tool, which uses the following plugins

* `autoprefixer` - postprecesses css with vendor prefixes
* `imagemin` - resizes and reformats images
* `jshint` - javascript linting
* `sourcemaps` - styles tracing in chrome
* `browser-sync` - loads browsersync for browser testing
* `shell`,`notify` - shell tools for drush and error catching
* `uglify`,`stylish`, `beautify`, `csscomb` - css formatting tools
* `sass`,`sass-glob` - sass processing

the `package.json` contains a full list of the files that are pulled into gulp.

## Resources
* Whiplash uses the [Google Web Starter Theme](https://developers.google.com/web/tools/starter-kit/?hl=en) as a skeleton.
* Whiplash uses [BEM-like styles](http://getbem.com/introduction/). Use [Harry Roberts's](http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/) naming conventions, as these are what Drupal 8 has adopted in their classy theme.
* [Foundation for Sites](https://github.com/zurb/foundation-sites-template)

d286568b23416950

