(function (Drupal) {
  jQuery(document).foundation();

    jQuery( "[data-history-node-id='3813'] a" ).attr('href','https://fcoe.org/departments/fccsdo');

    jQuery( ".video-grid ul li a" ).click(function(event) {
      event.preventDefault();
      vall = jQuery(this).attr("href");
      var videoid = vall.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
      //console.log(videoid);
      jQuery('#exampleModal8 iframe').attr('src', "https://www.youtube.com/embed/" + videoid[1] + "?autoplay=1&rel=0" );
      var newDescr = jQuery(this).siblings('.views-field-body').html();
      var newTitle = jQuery(this).siblings('.views-field-title').html();
      jQuery('.videogrid-descr').html( newDescr );
      jQuery('.videgrid-title span').html( "Education Matters - " + newTitle );
      jQuery('#exampleModal8').foundation('open');
    });

    jQuery( ".about-vid" ).click(function(event) {
      event.preventDefault();
      vall = jQuery(this).attr("href");
      var videoid = vall.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
      console.log(videoid);
      jQuery('#exampleModal8 iframe').attr('src', "https://www.youtube.com/embed/" + videoid[1] + "?autoplay=1&rel=0" );

      jQuery('#exampleModal8 iframe').foundation('open');
    });

    jQuery(document).on('closed.zf.reveal', '[data-reveal]', function () {
     jQuery('#exampleModal8 iframe').attr('src', '');
    });
})(Drupal);